/* picplanner-window.c
 *
 * Copyright 2021 user
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window/picplanner-window.h"
#include "window/overview-page/overview-view.h"
#include "window/sun-page/sun-view.h"
#include "window/moon-page/moon-view.h"
#include "window/milky-way-page/milky-way-view.h"
#include "calculations/calculations_sun.h"
#include "calculations/calculations_moon.h"
#include "calculations/calculations_milky_way.h"


struct _PicplannerWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar      *header_bar;
  GtkWidget         *box_main;
  GtkWidget         *stack;
  GtkWidget         *search_bar;    /* Search bar for the location */
  GtkWidget         *search_entry;  /* The search entry inside the search bar */
  GtkWidget         *search_button; /* The search button in the header bar so show the search bar */
  GtkWidget         *overview_box;  /* The overview page */
  GtkWidget         *sun_box;       /* The sun page */
  GtkWidget         *moon_box;      /* The moon page */
  GtkWidget         *milky_way_box; /* The milky way page */
  GSettings         *settings;      /* All settings of PicPlanner */

  /* Variables to limit the amount of user searches on can make per second */
  gboolean          search_new;
  gint              search_count;
  guint             search_timeout_id;
};

G_DEFINE_TYPE (PicplannerWindow, picplanner_window, ADW_TYPE_APPLICATION_WINDOW)


static void
websearch (PicplannerWindow *window)
{
  g_print("Searching...\n");
  char uri_string[200];
  const gchar *nominatim_url = "https://nominatim.openstreetmap.org/search?q=";
  const gchar *search_location = gtk_editable_get_text (GTK_EDITABLE (window->search_entry));
  const gchar *nominatim_settings = "&format=geojson";
  sprintf (uri_string, "%s%s%s", nominatim_url, search_location, nominatim_settings);
  GUri *uri = g_uri_parse (uri_string, G_URI_FLAGS_NONE, NULL);
  g_print ("%s\n", g_uri_to_string (uri));

  SoupSession *session = soup_session_new_with_options (SOUP_SESSION_USER_AGENT, "testagent",
                                                        NULL);
  SoupMessage *msg = soup_message_new (SOUP_METHOD_GET,g_uri_to_string(uri));
  soup_session_send_message (session, msg);

  JsonParser *parser;
  JsonNode *root;
  JsonObject *features;
  JsonArray *array;

  parser = json_parser_new ();
  json_parser_load_from_data (parser, msg->response_body->data, -1, NULL);
  root = json_parser_get_root (parser);
  features = json_node_get_object (root);
  array = json_object_get_array_member (features, "features");

  for (uint i = 0; i < json_array_get_length (array); i++)
    {
      JsonNode *node;
      JsonObject *object;
      JsonNode *node_properties;
      JsonObject *object_properties;
      const gchar *char_display_name;

      node = json_array_get_element (array, i);
      object = json_node_get_object (node);
      node_properties = json_object_get_member (object, "properties");
      object_properties = json_node_get_object (node_properties);
      char_display_name = json_object_get_string_member (object_properties, "display_name");
      g_print ("%s\n", char_display_name);

    }
}


static gboolean
search_timeout_signal (gpointer user_data)
{
  PicplannerWindow *window = user_data;
  if (window->search_count > 0)
    {
      window->search_count = 0;
    }
  else
    {
      window->search_new = TRUE;
      if (*gtk_editable_get_text (GTK_EDITABLE (window->search_entry)) != '\0')
          websearch(window);
      g_source_remove (window->search_timeout_id);
    }

  return TRUE;
}


static void
search_text_changed (GtkWidget *self,
                     PicplannerWindow *window)
{
  (void) self;
  if (window->search_new)
    {
      window->search_new = FALSE;
      window->search_timeout_id = g_timeout_add (SEARCH_TIMEOUT_LENGTH,
                                                 search_timeout_signal,
                                                 window);
    }
  window->search_count++;
}


static void
input_changed (GtkWidget *self,
               gpointer user_data)
{
  (void) self;
  PicplannerWindow  *window;

  int       *rise_upper_set_index_sun;
  int       *rise_upper_set_index_moon;
  int       *rise_upper_set_index_milky_way;
  int       *dark_blue_golden_index;
  double    *array_coordinates_sun;
  double    *array_coordinates_moon;
  double    *array_coordinates_milky_way;
  GDateTime *date_time;
  double    longitude, latitude;

  window = PICPLANNER_WINDOW (user_data);

  longitude = picplanner_overview_get_longitude (PICPLANNER_OVERVIEW (window->overview_box));
  latitude = picplanner_overview_get_latitude (PICPLANNER_OVERVIEW (window->overview_box));
  date_time = picplanner_overview_get_date_time (PICPLANNER_OVERVIEW (window->overview_box));

  /* Sun */

  array_coordinates_sun = picplanner_get_array_coordinates_sun (date_time,
                                                                longitude,
                                                                latitude);
  rise_upper_set_index_sun = picplanner_get_index_rise_upper_set (array_coordinates_sun);
  dark_blue_golden_index = picplanner_get_index_dark_blue_golden (array_coordinates_sun);

  picplanner_overview_set_current_coordinates_sun (PICPLANNER_OVERVIEW (window->overview_box),
                                                   array_coordinates_sun);
  picplanner_sun_set_rise_upper_set (date_time,
                                     array_coordinates_sun,
                                     rise_upper_set_index_sun);

  (void) dark_blue_golden_index;


  /* Moon */
  /*
  array_coordinates_moon = picplanner_get_array_coordinates_moon (date_time,
                                                                  longitude,
                                                                  latitude);
  rise_upper_set_index_moon = picplanner_get_index_rise_upper_set (array_coordinates_moon);
  picplanner_moon_set_rise_upper_set (date_time,
                                      array_coordinates_sun,
                                      rise_upper_set_index_moon);
  */
  /* Milky Way */
  /*
  array_coordinates_milky_way = picplanner_get_array_coordinates_milky_way (date_time,
                                                                            longitude,
                                                                            latitude);
  rise_upper_set_index_milky_way = picplanner_get_index_rise_upper_set (array_coordinates_milky_way);
  picplanner_milky_way_set_rise_upper_set (date_time,
                                           array_coordinates_sun,
                                           rise_upper_set_index_milky_way);
  */
  g_free (rise_upper_set_index_sun);
  //g_free (rise_upper_set_index_moon);
  //g_free (rise_upper_set_index_milky_way);
  g_free (dark_blue_golden_index);
  g_free (array_coordinates_sun);
  //g_free (array_coordinates_moon);
  //g_free (array_coordinates_milky_way);
}


static void
picplanner_window_class_init (PicplannerWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (PICPLANNER_OVERVIEW_TYPE);
  g_type_ensure (PICPLANNER_SUN_TYPE);
  g_type_ensure (PICPLANNER_MOON_TYPE);
  g_type_ensure (PICPLANNER_MILKYWAY_TYPE);

  gtk_widget_class_set_template_from_resource (widget_class, "/de/zwarf/picplanner/window/picplanner-window.ui");
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, box_main);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, search_bar);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, search_entry);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, search_button);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, overview_box);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, sun_box);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, moon_box);
  gtk_widget_class_bind_template_child (widget_class, PicplannerWindow, milky_way_box);

  gtk_widget_class_bind_template_callback (widget_class, search_text_changed);
}


static void
picplanner_window_init (PicplannerWindow *window)
{
  gtk_widget_init_template (GTK_WIDGET (window));

  window->settings = g_settings_new ("de.zwarf.picplanner");

  g_object_bind_property (window->search_button, "active",
                          window->search_bar, "search-mode-enabled",
                          G_BINDING_BIDIRECTIONAL);

  g_signal_connect (G_OBJECT (window->overview_box), "input-changed", G_CALLBACK (input_changed), window);

  window->search_new = true;
  window->search_count = 0;
  window->search_timeout_id = 0;
}
