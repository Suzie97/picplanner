#include "sun-view.h"


struct _PicplannerSun
{
  GtkBox parent_instance;
};

G_DEFINE_TYPE (PicplannerSun, picplanner_sun, GTK_TYPE_BOX)


void
picplanner_sun_set_rise_upper_set (GDateTime *date_time,
                                   double *coordinates_array,
                                   int *index_rise_upper_set)
{
  GDateTime *date_time_rise;
  GDateTime *date_time_upper;
  GDateTime *date_time_set;
  (void) coordinates_array;

  date_time_rise = g_date_time_add_minutes (date_time, index_rise_upper_set[0]*24*60/NUM_DATA_POINTS-12*60);
  date_time_upper = g_date_time_add_minutes (date_time, index_rise_upper_set[1]*24*60/NUM_DATA_POINTS-12*60);
  date_time_set = g_date_time_add_minutes (date_time, index_rise_upper_set[2]*24*60/NUM_DATA_POINTS-12*60);

  g_date_time_unref (date_time_rise);
  g_date_time_unref (date_time_upper);
  g_date_time_unref (date_time_set);
}


static void
picplanner_sun_init (PicplannerSun *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
picplanner_sun_class_init (PicplannerSunClass *class)
{
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/sun-page/sun-view.ui");
}

PicplannerSun *
picplanner_sun_new ()
{
  return g_object_new (PICPLANNER_SUN_TYPE, NULL);
}
