#include <gtk/gtk.h>
#include "window/picplanner-window.h"
#include "picplanner-application.h"
#include "calculations/calculations_transformations.h"

G_BEGIN_DECLS

#define PICPLANNER_SUN_TYPE (picplanner_sun_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerSun, picplanner_sun, PICPLANNER, SUN, GtkBox)


void
picplanner_sun_set_rise_upper_set (GDateTime *date_time,
                                   double *coordinates_array,
                                   int *index_rise_upper_set);


PicplannerSun *picplanner_sun_new ();

G_END_DECLS
