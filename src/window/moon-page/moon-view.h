#include <gtk/gtk.h>
#include "window/picplanner-window.h"
#include "picplanner-application.h"
#include "calculations/calculations_transformations.h"

G_BEGIN_DECLS

#define PICPLANNER_MOON_TYPE (picplanner_moon_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerMoon, picplanner_moon, PICPLANNER, MOON, GtkBox)


void
picplanner_moon_set_rise_upper_set (GDateTime *date_time,
                                    double *coordinates_array,
                                    int *index_rise_upper_set);


PicplannerMoon *picplanner_moon_new ();

G_END_DECLS
