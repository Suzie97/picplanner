#include "moon-view.h"


struct _PicplannerMoon
{
  GtkBox parent_instance;
};

G_DEFINE_TYPE (PicplannerMoon, picplanner_moon, GTK_TYPE_BOX)


void
picplanner_moon_set_rise_upper_set (GDateTime *date_time,
                                    double *coordinates_array,
                                    int *index_rise_upper_set)
{
  GDateTime *date_time_rise;
  GDateTime *date_time_upper;
  GDateTime *date_time_set;
  (void) coordinates_array;

  date_time_rise = g_date_time_add_minutes (date_time, index_rise_upper_set[0]*24*60/NUM_DATA_POINTS-12*60);
  date_time_upper = g_date_time_add_minutes (date_time, index_rise_upper_set[1]*24*60/NUM_DATA_POINTS-12*60);
  date_time_set = g_date_time_add_minutes (date_time, index_rise_upper_set[2]*24*60/NUM_DATA_POINTS-12*60);

  g_date_time_unref (date_time_rise);
  g_date_time_unref (date_time_upper);
  g_date_time_unref (date_time_set);
}


static void
picplanner_moon_init (PicplannerMoon *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
picplanner_moon_class_init (PicplannerMoonClass *class)
{
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/moon-page/moon-view.ui");
}

PicplannerMoon *
picplanner_moon_new ()
{
  return g_object_new (PICPLANNER_MOON_TYPE, NULL);
}
