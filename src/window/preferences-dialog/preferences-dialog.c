#include "preferences-dialog.h"


struct _PicplannerPrefs
{
  GtkDialog parent_instance;
  GSettings *settings;
  GtkWidget *timezone;
  GtkWidget *websearch;
};

G_DEFINE_TYPE (PicplannerPrefs, picplanner_prefs, GTK_TYPE_DIALOG)

static void
picplanner_prefs_init (PicplannerPrefs *prefs)
{
  gtk_widget_init_template (GTK_WIDGET (prefs));
  prefs->settings = g_settings_new ("de.zwarf.picplanner");

  g_settings_bind (prefs->settings, "time-zone",
                   prefs->timezone, "value",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (prefs->settings, "web-search",
                   prefs->websearch, "active",
                   G_SETTINGS_BIND_DEFAULT);
}

static void
picplanner_prefs_dispose (GObject *object)
{
  PicplannerPrefs *prefs;

  prefs = PICPLANNER_PREFS (object);

  g_clear_object (&prefs->settings);

  G_OBJECT_CLASS (picplanner_prefs_parent_class)->dispose (object);
}

static void
picplanner_prefs_class_init (PicplannerPrefsClass *class)
{
  G_OBJECT_CLASS (class)->dispose = picplanner_prefs_dispose;

  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/preferences-dialog/preferences-dialog.ui");
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, timezone);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, websearch);
}

PicplannerPrefs *
picplanner_prefs_new (PicplannerWindow *win)
{
  return g_object_new (PICPLANNER_PREFS_TYPE, "transient-for", win, "use-header-bar", TRUE, NULL);
}
