#include <gtk/gtk.h>
#include "window/picplanner-window.h"
#include "picplanner-application.h"
#include "calculations/calculations_transformations.h"

G_BEGIN_DECLS

#define PICPLANNER_MILKYWAY_TYPE (picplanner_milkyway_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerMilkyway, picplanner_milkyway, PICPLANNER, MILKYWAY, GtkBox)

void
picplanner_milky_way_set_rise_upper_set (GDateTime *date_time,
                                         double *coordinates_array,
                                         int *index_rise_upper_set);


PicplannerMilkyway *picplanner_milkyway_new ();

G_END_DECLS
