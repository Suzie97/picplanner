#include <gtk/gtk.h>
#include <shumate/shumate.h>
#include "window/picplanner-window.h"
#include "picplanner-application.h"

G_BEGIN_DECLS

#define PICPLANNER_OVERVIEW_TYPE (picplanner_overview_get_type ())
G_DECLARE_FINAL_TYPE (PicplannerOverview, picplanner_overview, PICPLANNER, OVERVIEW, GtkBox)

PicplannerOverview
*picplanner_overview_new ();

double
picplanner_overview_get_longitude (PicplannerOverview *overview);

double
picplanner_overview_get_latitude (PicplannerOverview *overview);

GDateTime
*picplanner_overview_get_date_time (PicplannerOverview *overview);

void
picplanner_overview_set_current_coordinates_sun (PicplannerOverview *overview, double *coordinates_sun);

void
picplanner_overview_set_current_coordinates_moon (PicplannerOverview *overview, double *coordinates_moon);

void
picplanner_overview_set_current_coordinates_milky_way (PicplannerOverview *overview, double *coordinates_milky_way);

G_END_DECLS
