#include "overview-view.h"
#include "calculations/calculations_transformations.h"
#include "calculations/calculations_sun.h"
#include "calculations/calculations_moon.h"
#include "calculations/calculations_milky_way.h"


/*
 * The time of no input of map movement that has to pass until a calculation of the positions
 * of sun, moon or milky way starts.
 */
#define MAP_MOVE_TIMEOUT_LENGTH 400


static guint signal_input_changed;


struct _PicplannerOverview
{
  GtkBox            parent_instance;
  GtkWidget         *calendar;
  GtkWidget         *calendar_popover;
  GtkWidget         *calendar_button;
  GtkWidget         *north_entry;
  GtkWidget         *east_entry;
  GtkWidget         *spin_button_hour;
  GtkWidget         *spin_button_minute;
  ShumateSimpleMap  *map;
  ShumateViewport   *viewport;

  gint               input_count;
  guint              input_timeout_id;
  gboolean           input_new;

  GDateTime         *date_time;
  double             longitude;
  double             latitude;

  GSettings         *settings;
};


G_DEFINE_TYPE (PicplannerOverview, picplanner_overview, GTK_TYPE_BOX)


/*
 * Create all the "get" functions.
 * These are used so the main window function can collect all the information inputed
 * by the user on the overview page after it received the input-changed signal.
 */
double
picplanner_overview_get_longitude (PicplannerOverview *overview)
{
  return overview->longitude;
}

double
picplanner_overview_get_latitude (PicplannerOverview *overview)
{
  return overview->latitude;
}

GDateTime
*picplanner_overview_get_date_time (PicplannerOverview *overview)
{
  return overview->date_time;
}

void
picplanner_overview_set_current_coordinates_sun (PicplannerOverview *overview, double *coordinates_sun)
{
  (void) overview;
  (void) coordinates_sun;
}

void
picplanner_overview_set_current_coordinates_moon (PicplannerOverview *overview, double *coordinates_moon)
{
  (void) overview;
  (void) coordinates_moon;
}

void
picplanner_overview_set_current_coordinates_milky_way (PicplannerOverview *overview, double *coordinates_milky_way)
{
  (void) overview;
  (void) coordinates_milky_way;
}


/*
 * Changing the date_time variable and longitude/latitude after a user input was recognized.
 * Afterwards, emit a signal to start the calculations.
 */
static void
change_date_time_position (PicplannerOverview *overview)
{
  int year, month, day, hour, minute;
  GDateTime *date_selected;

  /*
   * Get year, month and day information from the calendar widget.
   */
  date_selected = gtk_calendar_get_date (GTK_CALENDAR (overview->calendar));
  year = g_date_time_get_year (date_selected);
  month = g_date_time_get_month (date_selected);
  day = g_date_time_get_day_of_month (date_selected);

  /*
   * Get hour and minute from the spin buttons.
   */
  hour = gtk_spin_button_get_value (GTK_SPIN_BUTTON (overview->spin_button_hour));
  minute = gtk_spin_button_get_value (GTK_SPIN_BUTTON (overview->spin_button_minute));

  /*
   * Save the information into the date_time variable.
   */
  overview->date_time = g_date_time_new (g_date_time_get_timezone (overview->date_time),
                                         year, month, day,
                                         hour, minute,
                                         0);

  /*
   * Get the longitude and latitude from the input and save the information.
   */
  overview->longitude = shumate_location_get_longitude (SHUMATE_LOCATION (overview->viewport));
  overview->latitude = shumate_location_get_latitude (SHUMATE_LOCATION (overview->viewport));

  g_print("Date: %02d.%02d.%02d, Time: %02d:%02d\n", day, month, year, hour, minute);
  g_print ("Coordinates changed to: N %f, E %f \n", overview->latitude, overview->longitude);

  /*
   * Emit a signal that the user changed the input.
   */
  g_signal_emit (overview, signal_input_changed, 0, NULL);

  g_date_time_unref (date_selected);
}


/*
 * input_changed:
 * Function to call if the user changes the location or date/time.
 * At the beginning input_new is TRUE because a new input starts.
 * The input_new variable is set to FALSE if a user input is detected, and a timer is started.
 * The input_count is increased from now on by one, every time a user input is detected.
 *
 * input_timeout_signal:
 * If the timer has been running out it is checked if the counter was increased during the time
 * or not. If it has been increased it is reseted to 0 and the timer continues.
 * If no input was detected in the meantime (input_counter==0) a new detection series is
 * initialized by setting input_new = TRUE. Furthermore the userinput is saved with
 * change_date_time_position and the timer is deactivated.
 */
static gboolean
input_timeout_signal (gpointer user_data)
{
  PicplannerOverview *overview = user_data;
  if (overview->input_count == 0)
    {
      overview->input_new = TRUE;
      change_date_time_position(overview);
      g_source_remove (overview->input_timeout_id);
    }
  overview->input_count = 0;

  return TRUE;
}
static void
input_changed (GtkWidget *self,
               PicplannerOverview *overview)
{
  (void) self;

  if (overview->input_new)
  {
    overview->input_new = FALSE;
    overview->input_timeout_id = g_timeout_add (MAP_MOVE_TIMEOUT_LENGTH,
                                                input_timeout_signal,
                                                overview);
  }
  overview->input_count++;
}


/*
 * Change the text of the GtkSpinButton to always be a two digit number.
 * E.g. don't show one o'clock AM as 1:0 but show 01:00
 */
static gboolean
time_spin_button_text (GtkSpinButton *self)
{
  int value;
  char *button_text;
  GtkAdjustment *adjustment;

  adjustment = gtk_spin_button_get_adjustment (self);
  value = (int) gtk_adjustment_get_value (adjustment);
  button_text = g_strdup_printf ("%02d", value);
  gtk_editable_set_text (GTK_EDITABLE (self),
                         button_text);
  g_free (button_text);

  return TRUE;
}


/*
 * Change the text of the button which presents the calender after a date was selected.
 * Furthermore the behavior of the popup is controlled.
 */
static void
day_selected (GtkCalendar *self, PicplannerOverview *overview)
{
  (void) self;
  GDateTime *date_selected;
  GtkPopover *popover;
  char *button_text;

  date_selected = gtk_calendar_get_date (GTK_CALENDAR (overview->calendar));

  /*
   * Set text of the button to the selected date
   */
  button_text = g_strdup_printf ("%d.%d.%d",
                                 g_date_time_get_day_of_month (date_selected),
                                 g_date_time_get_month (date_selected),
                                 g_date_time_get_year (date_selected));
  gtk_menu_button_set_label (GTK_MENU_BUTTON (overview->calendar_button),
                             button_text);

  /*
   * Close Popover after the day was selected
   */
  popover = GTK_POPOVER(overview->calendar_popover);
  gtk_popover_popdown (popover);

  g_free (button_text);
  g_date_time_unref (date_selected);
}


static void
picplanner_overview_init (PicplannerOverview *overview)
{
  GTimeZone *timezone;
  ShumateMapSource *map_source;
  ShumateMapSourceRegistry *registry;

  gtk_widget_init_template (GTK_WIDGET (overview));

  overview->settings = g_settings_new ("de.zwarf.picplanner");

  /*
   * Setting the date and time of the date and time selector to the current date and time
   * with the timezone set in preferences
   */
  timezone = g_time_zone_new_offset (g_settings_get_int (overview->settings, "time-zone") * 3600);
  overview->date_time = g_date_time_new_now (timezone);
  gtk_calendar_select_day (GTK_CALENDAR (overview->calendar), overview->date_time);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (overview->spin_button_hour),
                             g_date_time_get_hour (overview->date_time));
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (overview->spin_button_minute),
                             g_date_time_get_minute (overview->date_time));

  /*
   * Initialisation of values needed to detect when a movement of the map ends
   */
  overview->input_new = true;
  overview->input_count = 0;
  overview->input_timeout_id = 0;

  /*
   * Initialise values necessary for displaying the map
   */
  registry = shumate_map_source_registry_new_with_defaults ();
  map_source = shumate_map_source_registry_get_by_id (registry, SHUMATE_MAP_SOURCE_OSM_MAPNIK);
  shumate_simple_map_set_map_source (overview->map, map_source);
  overview->viewport = shumate_simple_map_get_viewport (overview->map);

  /*
   * Make all the bindings between widget properties and g_settings.
   */
  g_settings_bind (overview->settings, "longitude",
                   overview->viewport, "longitude",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (overview->settings, "latitude",
                   overview->viewport, "latitude",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (overview->settings, "zoom-level",
                   overview->viewport, "zoom-level",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (overview->settings, "longitude",
                   overview->east_entry, "value",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (overview->settings, "latitude",
                   overview->north_entry, "value",
                   G_SETTINGS_BIND_DEFAULT);

  /*
   * Connect the signals which recognize user input.
   * The user inputs are:
   * Changes of: north coordinate, east coordinate, hour, minute and a date change in the calendar.
   * The map is not treated seperately because the coordinates of the map are bound to the entry
   * values above.
   */
  g_signal_connect (G_OBJECT (overview->north_entry),
                    "value-changed",
                    G_CALLBACK (input_changed),
                    overview);

  g_signal_connect (G_OBJECT (overview->east_entry),
                    "value-changed",
                    G_CALLBACK (input_changed),
                    overview);

  g_signal_connect (G_OBJECT (overview->spin_button_hour),
                    "value-changed",
                    G_CALLBACK (input_changed),
                    overview);

  g_signal_connect (G_OBJECT (overview->spin_button_minute),
                    "value-changed",
                    G_CALLBACK (input_changed),
                    overview);

  g_signal_connect (G_OBJECT (overview->calendar),
                    "day-selected",
                    G_CALLBACK (input_changed),
                    overview);
}

static void
picplanner_overview_class_init (PicplannerOverviewClass *class)
{
  g_type_ensure (SHUMATE_TYPE_SIMPLE_MAP);

  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/overview-page/overview-view.ui");

  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, calendar);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, calendar_popover);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, calendar_button);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, north_entry);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, east_entry);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, spin_button_hour);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, spin_button_minute);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerOverview, map);

  gtk_widget_class_bind_template_callback (class, day_selected);
  gtk_widget_class_bind_template_callback (class, time_spin_button_text);

  /*
   * Define the signal that is emitted if the user makes an input.
   * The signal is emitted (in the change_date_time_position function) after a limiter
   * that reduces the amount of signals that can be emitted in a specific time interval.
   */
  signal_input_changed = g_signal_new ("input-changed",
                                       G_OBJECT_CLASS_TYPE (class),
                                       G_SIGNAL_RUN_LAST,
                                       0, NULL, NULL, NULL,
                                       G_TYPE_NONE,
                                       0);
}

PicplannerOverview *
picplanner_overview_new ()
{
  return g_object_new (PICPLANNER_OVERVIEW_TYPE, NULL);
}
