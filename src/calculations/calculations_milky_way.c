#include "calculations_milky_way.h"


double
*picplanner_get_coordinates_rotational_milky_way (void)
{
  double right_ascension = 266.417;
  double declination = -29.008;
  double *coordinates_rotational = malloc (sizeof (double) * 2);
  /* Coordinates are nearly fixed compared to the non rotating coordinate system of the earth.
   * Values taken from Wikipedia? TODO*/

  coordinates_rotational[0] = right_ascension;
  coordinates_rotational[1] = declination;

  return coordinates_rotational;
}


double
*picplanner_get_coordinates_milky_way (GDateTime *date_time,
                                       double longitude,
                                       double latitude)
{
  double siderial_time;
  double *coordinates_milky_way;
  double *coordinates_horizontal_milky_way = malloc (sizeof (double) * 2 * NUM_DATA_POINTS);

  coordinates_milky_way = picplanner_get_coordinates_rotational_milky_way ();
  siderial_time = time_jd_to_sidereal_time (longitude, date_time);
  coordinates_horizontal_milky_way = picplanner_transform_rotational_to_horizontal(coordinates_milky_way,
                                                                                   latitude,
                                                                                   siderial_time);

  g_free (coordinates_milky_way);

  return coordinates_horizontal_milky_way;
}


double
*picplanner_get_array_coordinates_milky_way (GDateTime *date_time,
                                             double longitude,
                                             double latitude)
{
  GDateTime *iteration_time;
  double *coordinates_milky_way;
  double *array_coordinates_milky_way = malloc (sizeof (double) * 2 * NUM_DATA_POINTS);

  iteration_time = g_date_time_add_hours (date_time, -12);

  for (int i=0; i<NUM_DATA_POINTS; i++)
    {
      coordinates_milky_way = picplanner_get_coordinates_milky_way (iteration_time,
                                                                    longitude,
                                                                    latitude);
      array_coordinates_milky_way[2*i] = coordinates_milky_way[0];
      array_coordinates_milky_way[2*i+1] = coordinates_milky_way[1];
      iteration_time = g_date_time_add_minutes (iteration_time, 24*60/NUM_DATA_POINTS);
    }
  g_date_time_unref (iteration_time);
  g_free (coordinates_milky_way);

  return array_coordinates_milky_way;
}
