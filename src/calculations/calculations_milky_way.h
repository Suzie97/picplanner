#include "calculations_transformations.h"


double
*picplanner_get_coordinates_rotational_milky_way ();

double
*picplanner_get_coordinates_milky_way (GDateTime *date_time,
                                       double longitude,
                                       double latitude);

double
*picplanner_get_array_coordinates_milky_way (GDateTime *date_time,
                                             double longitude,
                                             double latitude);
