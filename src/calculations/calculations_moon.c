#include "calculations_moon.h"


double
*picplanner_get_coordinates_rotational_moon (GDateTime *date_time)
{
  double *coordinates_moon = malloc(sizeof (double) * 2);
  double right_ascension;
  double declination;
  double T;
  double mean_anomaly;
  double mean_longitude; /* ecliptic longitude? */
  double mean_distance;
  double longitude_moon;
  double latitude_moon;
  double ecliptic;
  double time_jd;

  time_jd = calc_jd (date_time);

  T = (time_jd-2451545.0)/36525.;

  mean_longitude = 218.316 + (13.176396*36525.) * T;
  mean_anomaly = 134.963 + (13.064993*36525.) * T;
  mean_distance = 93.272 + (13.229350*36525.) * T;

  longitude_moon = mean_longitude + 6.289 * sin(calc_deg_to_rad (mean_anomaly));
  latitude_moon = 5.128 * sin(calc_deg_to_rad(mean_distance));

  /*double distance_moon;
  distance_moon = 385001. - 20905. * cos(calc_deg_rad(mean_anomaly)); Useful for super moon calculation?*/

  ecliptic = calc_deg_to_rad(23.43928 + 0.01301*T);
  longitude_moon = calc_deg_to_rad (longitude_moon);
  latitude_moon = calc_deg_to_rad (latitude_moon);

  right_ascension = atan2(cos(ecliptic)*sin(longitude_moon)
                          -sin(ecliptic)*tan(latitude_moon), cos(longitude_moon));

  declination = asin(cos(ecliptic)*sin(latitude_moon)
                     +sin(ecliptic)*cos(latitude_moon)*sin(longitude_moon));

  coordinates_moon[0] = calc_rad_to_deg (right_ascension);
  coordinates_moon[1] = calc_rad_to_deg (declination);

  return coordinates_moon;
}


double
*picplanner_get_coordinates_moon (GDateTime *date_time,
                                  double longitude,
                                  double latitude)
{
  double siderial_time;
  double *coordinates_moon;
  double *coordinates_horizontal_moon = malloc (sizeof (double) * 2 * 288);

  coordinates_moon = picplanner_get_coordinates_rotational_moon (date_time);
  siderial_time = time_jd_to_sidereal_time (longitude, date_time);
  coordinates_horizontal_moon = picplanner_transform_rotational_to_horizontal (coordinates_moon,
                                                                               latitude,
                                                                               siderial_time);

  g_free (coordinates_moon);

  return coordinates_horizontal_moon;
}


double
*picplanner_get_array_coordinates_moon (GDateTime *date_time,
                                        double longitude,
                                        double latitude)
{
  GDateTime *iteration_time;
  double *coordinates_moon;
  double *array_coordinates_moon = malloc (sizeof (double) * 2 * NUM_DATA_POINTS);

  iteration_time = g_date_time_add_hours (date_time, -12);

  for (int i=0; i<NUM_DATA_POINTS; i++)
    {
      coordinates_moon = picplanner_get_coordinates_moon (iteration_time,
                                                          longitude,
                                                          latitude);
      array_coordinates_moon[2*i] = coordinates_moon[0];
      array_coordinates_moon[2*i+1] = coordinates_moon[1];
      iteration_time = g_date_time_add_minutes (iteration_time, 24*60/NUM_DATA_POINTS);
    }

  g_date_time_unref (iteration_time);
  g_free (coordinates_moon);

  return array_coordinates_moon;
}
