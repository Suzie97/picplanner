#include <time.h>
#include <math.h>
#include <glib.h>


/*
 * NUM_DATA_POINTS: The plots always show 24 hour. Therfore this value gives the amount of
 * datapoints which have to be calculated. (24*60)min / 5min = 288 --> Step every 5 min.
 */
#define NUM_DATA_POINTS 288


double
calc_deg_to_rad (double deg);

double
calc_rad_to_deg (double rad);

int
times_to_time_zone (int day_utc,
                    int hour_utc,
                    int day_local,
                    int hour_local);

int
*get_time_utc ();

int
*utc_time_to_local_time (int *time_utc);

int
*local_time_to_utc_time (int *time_local);

double
calc_jd (GDateTime *date_time);

double
time_jd_to_sidereal_time (double longitude,
                          GDateTime *date_time);

double
*picplanner_transform_rotational_to_horizontal (double *coordinates_rot,
                                                     double latitude,
                                                     double time_sidereal);

int
*picplanner_get_index_rise_upper_set (double *coordinates_array);

int
*picplanner_get_index_dark_blue_golden (double *coordinates_array);
