#include "calculations_transformations.h"


double
*picplanner_get_coordinates_rotational_moon (GDateTime *date_time);

double
*picplanner_get_coordinates_moon (GDateTime *date_time,
                                  double longitude,
                                  double latitude);

double
*picplanner_get_array_coordinates_moon (GDateTime *date_time,
                                        double longitude,
                                        double latitude);
