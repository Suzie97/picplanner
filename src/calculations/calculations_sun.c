#include "calculations_sun.h"


double
*picplanner_get_coordinates_rotational_sun (GDateTime *date_time)
{
  double *coordinates_sun = malloc(sizeof (double) * 2);
  double right_ascension;
  double declination;
  double T;
  double mean_longitude;
  double mean_anomaly;
  double equation_of_center;
  double ecliptic_longitude;
  double ecliptic;
  double time_jd;

  time_jd = calc_jd (date_time);

  T = (time_jd-2451545.0)/36525.;
  mean_longitude = 280.46645 + (36000.76983 + 0.0003032*T)*T;
  mean_anomaly = 357.52910 + (35999.05030 - (0.0001559 - 0.00000048*T)*T)*T;
  equation_of_center = (1.914600 - (0.004817 - 0.000014*T)*T)*sin(calc_deg_to_rad (mean_anomaly))
                     + (0.019993 - 0.000101*T)*sin(2*calc_deg_to_rad (mean_anomaly))
                     + (0.00029)*sin(3*calc_deg_to_rad (mean_anomaly));
  ecliptic_longitude = mean_longitude + equation_of_center;
  ecliptic = 23.43928 + 0.01301*T;

  ecliptic_longitude = calc_deg_to_rad (ecliptic_longitude);
  ecliptic = calc_deg_to_rad (ecliptic);

  right_ascension = atan2 (cos(ecliptic)*sin(ecliptic_longitude), cos(ecliptic_longitude));
  declination = asin (sin(ecliptic)*sin(ecliptic_longitude));

  coordinates_sun[0] = calc_rad_to_deg (right_ascension);
  coordinates_sun[1] = calc_rad_to_deg (declination);

  return coordinates_sun;
}


double
*picplanner_get_coordinates_sun (GDateTime *date_time,
                                 double longitude,
                                 double latitude)
{
  double siderial_time;
  double *coordinates_sun;
  double *coordinates_horizontal_sun = malloc (sizeof (double) * 2 * 288);


  coordinates_sun = picplanner_get_coordinates_rotational_sun (date_time);
  siderial_time = time_jd_to_sidereal_time (longitude, date_time);
  coordinates_horizontal_sun = picplanner_transform_rotational_to_horizontal (coordinates_sun,
                                                                              latitude,
                                                                              siderial_time);
  g_free (coordinates_sun);

  return coordinates_horizontal_sun;
}


double
*picplanner_get_array_coordinates_sun (GDateTime *date_time,
                                       double longitude,
                                       double latitude)
{
  GDateTime *iteration_time;
  double *coordinates_sun;
  double *array_coordinates_sun = malloc (sizeof (double) * 2 * NUM_DATA_POINTS);


  iteration_time = g_date_time_add_hours (date_time, -12);

  for (int i=0; i<NUM_DATA_POINTS; i++)
    {
      coordinates_sun = picplanner_get_coordinates_sun (iteration_time,
                                                        longitude,
                                                        latitude);

      array_coordinates_sun[2*i] = coordinates_sun[0];
      array_coordinates_sun[2*i+1] = coordinates_sun[1];
      iteration_time = g_date_time_add_minutes (iteration_time, 24*60/NUM_DATA_POINTS);

      g_free (coordinates_sun);
    }

  g_date_time_unref (iteration_time);

  return array_coordinates_sun;
}
