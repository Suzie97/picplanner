# PicPlanner

<img align='center' alt='PicPlanner Icon' src='https://gitlab.com/Zwarf/picplanner/-/raw/master/title-image.png'/>
<br/><br/> 
<p> A GTK application for photographers using GNU Linux or especially Linux phones. It can be used to calculate the position of the Sun, Moon and Milky Way in order to plan the position and time for a photograph.</p>

## The Idea
People who love to photograph know, the biggest problem is being at the right spot at the right time.
Therefore, landscape pictures should be planned before visiting the place of choice. To plan the location it is important to know the positon of the sun, moon and sometimes also the milky way.
Where to find the sun at which time is most of the time easy to guess but when is the sunset? And for the milky way normally nobody knows where to find it in the night sky at which time at a specific location.
This small program should answer all these questions.

## Plan
```diff
+1.  Create a basic GNOME like GUI -- Starting from zero again... Update: I think we can call it basic now :D
+2.  Calculate the center of the milky way at a specific time -- Implemented and equations seem to be very precise (± 1°) 
+3.  Calculate the position of the sun -- Implemented and equations seem to be very precise (< ± 1°) 
+4.  Find the position of the moon at a spesific time -- Implemented and equations seem to be sufficiently precise (~ ± 2°) 
-5.  Present all calculated data in the GUI
-6.  Create the web search -- Old method probably not very convenient. Starting new implementation with Libsoup
-7.  Plotting graphs is still a topic of discussion. Update: Probably "Cairo", more work but more "gtk like"
-8.  Implement "smarphone functionality" e.g. find location via GPS
+9.  Insert a map view with OSM
-10. Visualise important data direktly on the map
```

## Formulas used

Most of the formulas used are explained in detail directly inside the code. Reference is given or will be added soon.
Some of these formulas are from the excellent book by Jean Meeus "Astronomical Algorithms". Others are from scientific papers or combined by me.


## Libraries

- GTK 4 is used to create the GUI.
- Libadwaita will be used to make the GUI adaptive.
- GDK and Cairo will probably be used for the plots.
- Libsoup is probably used to connect to "Nominatim" (OpenStreetMap) to translate searched words into coordinates.
- Shumate is used to show a map.


## Build

At the moment this application is NOT fully functional.

```diff
git clone https://gitlab.com/Zwarf/picplanner.git
cd picplanner
meson builddir
cd builddir
ninja
ninja install
```
## Screenshots

New screenshots from the current state of the application. There will be significant changes as soon as libadwaita is released.

<a href='https://gitlab.com/zwarf/picplanner/-/blob/master/screenshots/screenshot-5.png'><img height='300px' alt='screenshot-large' src='https://gitlab.com/zwarf/picplanner/-/raw/master/screenshots/screenshot-5.png'/></a>


These screenshots are from the previous GTK3 application which never finished. They are here to illustrate what the basic idea of the app is.

<a href='https://gitlab.com/zwarf/picplanner/-/blob/master/screenshots/screenshot-1.png'><img height='300px' alt='screenshot-large' src='https://gitlab.com/zwarf/picplanner/-/raw/master/screenshots/screenshot-1.png'/></a>
<a href='https://gitlab.com/zwarf/picplanner/-/blob/master/screenshots/screenshot-2.png'><img height='300px' alt='screenshot-large' src='https://gitlab.com/zwarf/picplanner/-/raw/master/screenshots/screenshot-2.png'/></a>
<a href='https://gitlab.com/zwarf/picplanner/-/blob/master/screenshots/screenshot-3.png'><img height='300px' alt='screenshot-large' src='https://gitlab.com/zwarf/picplanner/-/raw/master/screenshots/screenshot-3.png'/></a>
<a href='https://gitlab.com/zwarf/picplanner/-/blob/master/screenshots/screenshot-4.png'><img height='300px' alt='screenshot-large' src='https://gitlab.com/zwarf/picplanner/-/raw/master/screenshots/screenshot-4.png'/></a>
